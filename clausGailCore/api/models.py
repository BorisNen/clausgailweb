from django.db import models


class GailResponse(models.Model):
    fiveYearABS = models.CharField(max_length=100, default='')
    fiveYearAVE = models.CharField(max_length=100, default='')
    lifetimeABS = models.CharField(max_length=100, default='')
    lifetimeAve = models.CharField(max_length=100, default='')


class ClausResponse(models.Model):
    result = models.CharField(max_length=100, default='')
