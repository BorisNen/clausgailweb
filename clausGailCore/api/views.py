from django.core import serializers
from django.http.response import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from calculator import calculator_api as calc

from api.models import GailResponse, ClausResponse


@api_view(['GET'])
def test(request):
    return JsonResponse("Working", safe=False)

@api_view(['POST'])
def gail(request):
    fiveYearABS, fiveYearAVE, lifetimeABS, lifetimeAve  = calc.run_gail(request.data.get("age"), request.data.get("biopsies"),
                                                                        request.data.get("had_biopsies"), request.data.get("hyperplasia"),
                                                                        request.data.get("period"), request.data.get("birth"),
                                                                        request.data.get("relatives"), request.data.get("race"))
    data = serializers.serialize('json', [GailResponse(fiveYearABS=fiveYearABS*100, fiveYearAVE=fiveYearAVE*100,
                                                   lifetimeABS=lifetimeABS*100, lifetimeAve=lifetimeAve*100)],
                                 fields=('fiveYearABS', 'fiveYearAVE', 'lifetimeABS', 'lifetimeAve'))
    return HttpResponse(data, content_type="application/json")


@api_view(['POST'])
def claus(request):
    mother, daughter, sister, maunt, paunt, mgrandmother, pgrandmother, \
    mstepsister, pstepsister = None, None, None, None, None, None, None, None, None

    if request.data.get("mother_age"):
        mother = int(request.data.get("mother_age"))
    if request.data.get("daughter_age"):
        daughter = [int(request.data.get("daughter_age"))]
    if request.data.get("sister_age"):
        sister = [int(request.data.get("sister_age"))]
    if request.data.get("maunt_age"):
        maunt = [int(request.data.get("maunt_age"))]
    if request.data.get("paunt_age"):
        paunt = [int(request.data.get("paunt_age"))]
    if request.data.get("mgrandmother_age"):
        mgrandmother = [int(request.data.get("mgrandmother_age"))]
    if request.data.get("pgrandmother_age"):
        pgrandmother = [int(request.data.get("pgrandmother_age"))]
    if request.data.get("mhsister_age"):
        mstepsister = [int(request.data.get("mhsister_age"))]
    if request.data.get("phsister_age"):
        pstepsister = [int(request.data.get("phsister_age"))]

    result = calc.run_claus(int(request.data.get("age")), mother, daughter, sister, maunt, paunt, mgrandmother,
                            pgrandmother, mstepsister, pstepsister)
    data = serializers.serialize('json', [ClausResponse(result=result*100)], fields=('result'))
    return HttpResponse(data, content_type="application/json")
