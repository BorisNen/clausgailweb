from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^api/test$', views.test),
    url(r'^api/gail$', views.gail),
    url(r'^api/claus$', views.claus)
]