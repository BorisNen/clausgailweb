from rest_framework import serializers
from api.models import GailResponse, ClausResponse


class GailResponseResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = GailResponse
        fields = ('fiveYearABS',
                  'fiveYearAVE',
                  'lifetimeABS',
                  'lifetimeAve')


class ClausResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClausResponse
        fields = ('result')
