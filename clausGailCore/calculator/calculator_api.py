import calculator.claus_calculator as claus
import calculator.gail_calculator as gail
import numpy as np


def run_gail(age, biopsies, had_biopsies, atbiopsies, menarch, birth, relatives, race):
    if int(age) > 85:
        age = 85
    elif int(age) < 35:
        age = 35
    else:
        age = int(age)

    age_indicator = 0 if age < 50 else 1

    gailMod = gail.GailRiskCalculator()
    gailMod.Initialize()

    rhyp = np.float64(1.0)
    if had_biopsies == 1:
        if atbiopsies == 0:
            rhyp = np.float64(0.93)
        elif atbiopsies == 1:
            rhyp = np.float(1.82)

    fiveYearABS = gailMod.CalculateAbsoluteRisk(age,  # CurrentAge int    [t1]
                                                age + 5,  # ProjectionAge int    [t2]
                                                age_indicator,  # AgeIndicator int    [i0]
                                                biopsies,  # NumberOfBiopsy int    [i2]
                                                menarch,  # MenarcheAge int    [i1]
                                                birth,  # FirstLiveBirthAge int    [i3]
                                                had_biopsies,  # EverHaveBiopsy int [iever]
                                                relatives,  # FirstDegRelatives int    [i4]
                                                atbiopsies,
                                                # int[ihyp]HyperPlasia
                                                np.float64(1.82),  # double [rhyp]  RHyperPlasia
                                                race)  # irace int    [race]
    fiveYearAVE = gailMod.CalculateAeverageRisk(age,
                                                age + 5,
                                                age_indicator,
                                                biopsies,
                                                menarch,
                                                birth,
                                                had_biopsies,
                                                relatives,
                                                atbiopsies,
                                                rhyp,
                                                race)
    lifetimeABS = gailMod.CalculateAbsoluteRisk(age,
                                                90,
                                                age_indicator,
                                                biopsies,
                                                menarch,
                                                birth,
                                                had_biopsies,
                                                relatives,
                                                atbiopsies,
                                                rhyp,
                                                race)
    lifetimeAve = gailMod.CalculateAeverageRisk(age,
                                                90,
                                                age_indicator,
                                                biopsies,
                                                menarch,
                                                birth,
                                                had_biopsies,
                                                relatives,
                                                atbiopsies,
                                                rhyp,
                                                race)

    return fiveYearABS, fiveYearAVE, lifetimeABS, lifetimeAve


def run_claus(age, mother, daughter, sister, maunt, paunt, mgrandmother, pgrandmother,
             mstepsister, pstepsister):

    claus_result = claus.calculate_risk(age, mother, daughter, sister,
                                        maunt, paunt, mgrandmother, pgrandmother, mstepsister,
                                        pstepsister)
    if claus_result is None:
        claus_result = 0

    return claus_result
