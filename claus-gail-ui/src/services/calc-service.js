import axios from 'axios'
const API_URL = 'http://localhost:8000/api'

class CalculatorService {
  gailAPI (fd) {
    return axios.post(API_URL + '/gail', fd)
  }

  clausAPI (fd) {
    return axios.post(API_URL + '/claus', fd)
  }
}

export default new CalculatorService()
