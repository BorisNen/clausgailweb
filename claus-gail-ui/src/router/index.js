import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Gail from '../views/Gail.vue'
import Claus from '../views/Claus.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/gail',
    name: 'gail',
    component: Gail
  },
  {
    path: '/claus',
    name: 'claus',
    component: Claus
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
